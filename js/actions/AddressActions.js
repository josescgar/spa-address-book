//Libraries
var AppDispatcher = require('../dispatcher/AppDispatcher');

//Components
var AddressConstants = require('../constants/AddressConstants');

/**
 * Possible actions related to the address book store
 * that might happen within a view component
 */
var AddressActions = {
	createContact: function(firstName, lastName, email, country) {
		AppDispatcher.handleAction({
			actionType: AddressConstants.ACTION_CREATE_CONTACT,
			firstName: firstName,
			lastName: lastName,
			email: email,
			country: country,
			persist: true
		});
	},

	updateContact: function(contactId, firstName, lastName, email, country) {
		AppDispatcher.handleAction({
			actionType: AddressConstants.ACTION_UPDATE_CONTACT,
			contactId: contactId,
			firstName: firstName,
			lastName: lastName,
			email: email,
			country: country,
			persist: false
		});
	},

	editContact: function(contactId) {
		AppDispatcher.handleAction({
			actionType: AddressConstants.ACTION_EDIT_CONTACT,
			contactId: contactId
		});
	},

	deleteContact: function(contactId) {
		AppDispatcher.handleAction({
			actionType: AddressConstants.ACTION_DELETE_CONTACT,
			contactId: contactId
		});
	},

	stopEditing: function() {
		AppDispatcher.handleAction({
			actionType: AddressConstants.ACTION_FINISH_EDIT
		});
	},

	searchContacts: function(term) {
		AppDispatcher.handleAction({
			actionType: AddressConstants.ACTION_SEARCH_CONTACTS,
			search: term
		});
	}
};

module.exports = AddressActions;