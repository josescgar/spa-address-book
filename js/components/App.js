//Libraries
var React = require('react');

//Components
var ContactList = require('./contacts/ContactList');
var SearchContacts = require('./search/SearchContacts');
var ContactForm = require('./form/ContactForm');

/**
 * Entry application component. Sets the whole page layout
 * and includes all the initial view components
 **/
var App = React.createClass({

	render: function() {
		return(
			<div>
				<div className="col-md-8">
					<h1>My address book</h1>
					<SearchContacts/>
					<ContactList/>
				</div>
				<div className="col-md-4">
					<ContactForm/>
				</div>
			</div>
		);
	}
});

module.exports = App;