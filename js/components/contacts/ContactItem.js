//Libraries
var React = require('react');
var ReactDOM = require('react-dom');
var countries = require('country-list')();
var Highlight = require('react-highlighter');

//Components
var AddressActions = require('../../actions/AddressActions');

/**
 * View component for each element inside the contact list
 */
var ContactItem = React.createClass({
	render: function() {
		var fullName = this.props.contact.firstName + ' ' + this.props.contact.lastName;
		var email = this.props.contact.email;

		return (
			<div className="row" ref="contactCard">
				<div className="well well-sm">
					<h3><Highlight search={this.props.searchTerm} matchElement="u">{fullName}</Highlight></h3>
					<h4><Highlight search={this.props.searchTerm} matchElement="u">{email}</Highlight></h4>
					<p><small><i>{countries.getName(this.props.contact.country)}</i></small></p>
					<p>
						<span className="btn btn-xs btn-default" onClick={this._editContact}>Edit</span> <span className="btn btn-xs btn-danger" onClick={this._deleteContact}>Delete</span>
					</p>
				</div>
			</div>
		);
	},

	_editContact: function() {
		//Animate card being edited
		var card = ReactDOM.findDOMNode(this.refs.contactCard);
		var oldClassName = card.className;
		card.className += ' edit-contact';

		//Remove the animation after completion
		setTimeout(function() {
			card.className = oldClassName;
		}, 1000);

		//Notify store
		AddressActions.editContact(this.props.contact.id);
	},

	_deleteContact: function() {
		var fullName = this.props.contact.firstName + ' ' + this.props.contact.lastName;
		if (confirm('Are you sure you want to delete the contact information for ' + fullName + '?')) {
			AddressActions.deleteContact(this.props.contact.id);
		}
	}
});

module.exports = ContactItem;