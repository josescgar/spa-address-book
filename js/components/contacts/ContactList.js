//Libraries
var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');

//Components
var AddressStore = require('../../stores/AddressStore');
var ContactItem = require('./ContactItem');

/**
 * Internal component elements
 */
function getAddressState() {
	return {
		contacts: AddressStore.getContacts(),
		searchTerm: AddressStore.getSearchTerm(),
		justAdded: AddressStore.getJustAddedItem(),
		justDeleted: AddressStore.getJustDeletedItem()
	};
}

/**
 * Contact list view component entry point definition
 */
var ContactList = React.createClass({

	//Get initial React state for the view component
	getInitialState: function() {
		return getAddressState();
	},

	//Subscribe to the store after the component is ready
	componentDidMount: function () {
		AddressStore.addChangeListener(this._onAddressStoreChanged);
	},

	//Stop listening changes from the store
	componentWillUnmount: function () {
		AddressStore.removeChangeListener(this._onAddressStoreChanged)
	},

	//Something changed in the address store. Update component state.
	_onAddressStoreChanged: function() {
		this.setState(getAddressState());
	},

	//Render the component view
	render: function() {

		var filterText = this.state.searchTerm.toLowerCase();
		var contacts = this.state.contacts.map(function(contact) {
			//Check if we are filtering results by a search term
			if (typeof filterText === "string" && filterText.length > 0) {
				var fullName = contact.firstName + ' ' + contact.lastName;
				var isMatch = fullName.toLowerCase().indexOf(filterText) !== -1 
					|| contact.email.toLowerCase().indexOf(filterText) !== -1;
				if (!isMatch) {
					return;
				}
			}

			return <ContactItem contact={contact} key={contact.id} searchTerm={filterText}/>
		});

		var animateEnter = this.state.justAdded !== null;
		var animateExit = this.state.justDeleted !== null;

		return(
			<div>
				<div className="col-md-12">
					<ReactCSSTransitionGroup transitionName="animate-contact" transitionEnterTimeout={1000} transitionLeaveTimeout={1000} transitionEnter={animateEnter} transitionLeave={animateExit}>
						{contacts}
					</ReactCSSTransitionGroup>
				</div>
			</div>
		);
	}

});

module.exports = ContactList;