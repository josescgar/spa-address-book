//Libraries
var React = require('react');

//Components
var AddressActions = require('../../actions/AddressActions');
var AddressStore = require('../../stores/AddressStore');

function getSearchState() {
	return {
		term: AddressStore.getSearchTerm()
	};
}

/**
 * Contact search view component entry point definition
 */
var SearchContacts = React.createClass({

	getInitialState: function() {
		return getSearchState();
	},

	//Render the component view
	render: function() {
		return(
			<div className="row">
				<div className="col-md-12 form-group">
					<div className="input-group">
						<div className="input-group-addon"><span className="glyphicon glyphicon-search" aria-hidden="true"></span></div>
						<input type="text" valueLink={this.linkSearchValue('term')} className="form-control" placeholder="Search contacts..." />
					</div>
				</div>
			</div>
		);
	},

	linkSearchValue: function(key) {
		var that = this;
		return {
			value: this.state[key],
			requestChange: function(newValue) {
				AddressActions.searchContacts(newValue);
				that.setState(getSearchState());
			}
		}
	},
});

module.exports = SearchContacts;