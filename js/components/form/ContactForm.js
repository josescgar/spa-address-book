//Libraries
var React = require('react');
var ReactDOM = require('react-dom');
var countries = require('country-list')();

//Components
var AddressActions = require('../../actions/AddressActions');
var AddressStore = require('../../stores/AddressStore');

function getAddressState() {
	var contact = AddressStore.getContactBeingEdited();
	return {
		contact: contact,
		editing: contact !== null
	}
}

/**
 * Contact add/edit form view component entry point definition
 */
var ContactForm = React.createClass({

	//Get initial React state for the view component
	getInitialState: function() {
		return getAddressState();
	},

	//Subscribe to the store after the component is ready
	componentDidMount: function () {
		AddressStore.addChangeListener(this._onAddressStoreChanged);
	},

	//Stop listening changes from the store
	componentWillUnmount: function () {
		AddressStore.removeChangeListener(this._onAddressStoreChanged);
	},

	//Something changed in the address store. Update component state.
	_onAddressStoreChanged: function() {
		this.setState(getAddressState());
	},

	//Render the component view
	render: function() {
		//Prepare the items for the country select
		var countryNodes = countries.getCodes().map(function(countryCode) {
			return (
				<option value={countryCode} key={countryCode}>{countries.getName(countryCode)}</option>
			);
		});

		//Show the right texts for the form
		var title = this.state.editing ? 'Edit contact' : 'Add new contact';

		//Show the right form button
		var finishEditButton;
		var saveButton;
		if (this.state.editing) {
			finishEditButton = <button className="btn btn-default" onClick={this._stopEditing}>Finish editing</button>;
		} else {
			saveButton = <button type="submit" className="btn btn-success">Create contact</button>;
		}

		return(
			<div className="row">
				<h2>{title}</h2>
				<form onSubmit={this._handleSubmit}>
					<div className="form-group">
						<label htmlFor="firstname">First name:</label>
						<input type="text" className="form-control" id="firstname" valueLink={this.linkFormValue('firstName')} name="firstname" placeholder="Contact's first name" ref="firstname" required/>
					</div>
					<div className="form-group">
						<label htmlFor="lastname">Last name:</label>
						<input type="text" className="form-control" id="lastname" valueLink={this.linkFormValue('lastName')} name="lastname" placeholder="Contact's last name" ref="lastname" required/>
					</div>
					<div className="form-group">
						<label htmlFor="email">Email:</label>
						<input type="email" className="form-control" id="email" valueLink={this.linkFormValue('email')} name="email" placeholder="Contact's email address" ref="email" required/>
					</div>
					<div className="form-group">
						<label htmlFor="country">Country:</label>
						<select className="form-control" ref="country" name="country" valueLink={this.linkFormValue('country')} id="country" required>
							<option value="" key="none">Select a country...</option>
							{countryNodes}
						</select>
					</div>
					{saveButton}
					{finishEditButton}
				</form>
			</div>
		);
	},

	//Due to ReactJS nature, it won't allow changing data in the form if it is not linked to the state
	linkFormValue: function(key) {
		if (!this.state.editing) {
			return;
		}

		var that = this;
		return {
			value: this.state.contact[key],
			requestChange: function(newValue) {
				that.state.contact[key] = newValue;
				AddressActions.updateContact(
					that.state.contact.id,
					that.state.contact.firstName,
					that.state.contact.lastName,
					that.state.contact.email,
					that.state.contact.country
				);
			}
		}
	},

	_handleSubmit: function(event) {
		event.preventDefault();
		
		var firstName = ReactDOM.findDOMNode(this.refs.firstname).value.trim();
		var lastName = ReactDOM.findDOMNode(this.refs.lastname).value.trim();
		var email = ReactDOM.findDOMNode(this.refs.email).value.trim();
		var country = ReactDOM.findDOMNode(this.refs.country).value.trim();

		AddressActions.createContact(firstName, lastName, email, country);
		
		//Reset the form to avoid double clicks creating multiple entries
		event.target.reset();
	},

	_stopEditing: function(event) {
		AddressActions.stopEditing();
	}

});

module.exports = ContactForm;