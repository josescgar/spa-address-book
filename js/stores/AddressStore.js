//Libraries
var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var uuid = require('uuid');

//App modules
var AddressConstants = require('../constants/AddressConstants');

/**
 * STORE BUSSINESS LOGIC
 */
//Currently stored contacts
var _contacts = [];

//If editing a contact, the contact information
var _editContact = null;

//Current search term in the search box
var _searchTerm = '';

//Let views know that the given item has just been added or deleted
var _justAddedContactId = null;
var _justDeletedContactId = null;

//Get addresses from the web storage
function getFromStorage() {
	if (typeof localStorage.getItem(AddressConstants.STORAGE_ADDRESS_KEY) == "string") {
		var retrieved = JSON.parse(localStorage.getItem(AddressConstants.STORAGE_ADDRESS_KEY));
		if (retrieved) {
			_contacts = retrieved;
		}
	}
}

//Update the web storage
function persistInStorage() {
	localStorage.setItem(AddressConstants.STORAGE_ADDRESS_KEY, JSON.stringify(_contacts));
}

//Create a new contact
function saveContact(contactId, firstName, lastName, email, country, doPersist) {
	var contact = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		country: country
	};

	contact.id = contactId ? contactId : uuid.v4();
	var existing = getContactIndexById(contactId);
	var now = new Date().getTime();

	if (existing > -1) {
		contact.lastUpdate = now;
		_contacts[existing] = contact;
	} else {
		contact.createdAt = now;
		_contacts.push(contact);
	}
	
	if(doPersist) {
		persistInStorage();
	}
}

//Delete an existing contact
function deleteContact(contactId) {
	_contacts.splice(getContactIndexById(contactId), 1);
	persistInStorage();
}

//Helper functions
function getContactIndexById(contactId) {
	for (var i = 0; i < _contacts.length; i++) {
		if (_contacts[i].id === contactId) {
			return i;
		}
	}

	return -1;
}

/**
 * STORE DEFINITION
 **/
var AddressStore = assign(EventEmitter.prototype, {

	//Notify subscribed view components that store data has changed
	emitChange: function() {
		this.emit(AddressConstants.EVENT_STORE_CHANGED);
	},

	//Add a new view component to listen for changes in the store data
	addChangeListener: function(callback) {
		this.on(AddressConstants.EVENT_STORE_CHANGED, callback);
	},

	//Remove a view component from the store
	removeChangeListener: function(callback) {
		this.removeListener(AddressConstants.EVENT_STORE_CHANGED, callback);
	},

	getContacts: function() {
		return _contacts;
	},

	getContactBeingEdited: function() {
		return _editContact;
	},

	getSearchTerm: function() {
		return _searchTerm;
	},

	getJustAddedItem: function() {
		return _justAddedContactId;
	},

	getJustDeletedItem: function() {
		return _justDeletedContactId;
	}
});

/**
 * STORE INITIALIZATION
 */

//Read contacts stored in the local storage
getFromStorage();

//Listen for events comming from the central dispatcher
AppDispatcher.register(function(payload) {
	var action = payload.action;

	switch (action.actionType) {
		case AddressConstants.ACTION_CREATE_CONTACT:
			saveContact(action.contactId, action.firstName, action.lastName, action.email, action.country, action.persist);
			_justAddedContactId = action.contactId;
			AddressStore.emitChange();
			_justAddedContactId = null;
			break;
		case AddressConstants.ACTION_UPDATE_CONTACT:
			saveContact(action.contactId, action.firstName, action.lastName, action.email, action.country, action.persist);
			AddressStore.emitChange();
			break;
		case AddressConstants.ACTION_DELETE_CONTACT:
			deleteContact(action.contactId);
			_justDeletedContactId = action.contactId;
			AddressStore.emitChange();
			_justDeletedContactId = null;
			break;
		case AddressConstants.ACTION_EDIT_CONTACT:
			_editContact = _contacts[getContactIndexById(action.contactId)];
			AddressStore.emitChange();
			break;
		case AddressConstants.ACTION_FINISH_EDIT:
			_editContact = null;
			persistInStorage();
			AddressStore.emitChange();
			break;
		case AddressConstants.ACTION_SEARCH_CONTACTS:
			_searchTerm = action.search;
			AddressStore.emitChange();
			break;
	}

});

module.exports = AddressStore;