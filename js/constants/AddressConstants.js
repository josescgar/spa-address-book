var AddressConstants = {

	//Event triggered by the EventEmitter when data changes in the store
	EVENT_STORE_CHANGED: 'address-store-changed',

	//Key name for storing addresses in the web storage
	STORAGE_ADDRESS_KEY: 'contacts',

	//Action triggered when saving a new contact
	ACTION_CREATE_CONTACT: 'contact-create',

	//Action triggered when a contact from the list is selected for edition
	ACTION_EDIT_CONTACT: 'contact-edit',

	//Action triggered when decided to stop editing a contact
	ACTION_FINISH_EDIT: 'contact-stop-edit',

	//Action triggered when a contact is confirmed for deletion
	ACTION_EDIT_CONTACT: 'contact-delete',

	//Action triggered when changing the information for a given contact
	ACTION_UPDATE_CONTACT: 'contact-update',

	//Search contacts from the address book
	ACTION_SEARCH_CONTACTS: 'contact-search'
};

module.exports = AddressConstants;