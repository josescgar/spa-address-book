var gulp = require('gulp');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

//Source and compiled versions directories
var config = {
	source: {
		sass: './sass/base.scss',
		js: './js/app.js'
	},
	dest: {
		folder: './public/'
	}
};

/**
 * Usually it is a good idea to have a separate 'vendor.js' for libraries since
 * they are the least likely to change, therefore leveraging the client cache
 */
var externalLibs = [
	'country-list',
	'flux',
	'react',
	'react-dom',
	'react-addons-css-transition-group',
	'react-highlighter',
	'uuid',
	'events',
	'object-assign'
];

//Main Javascript application (App bussiness logic)
gulp.task('js-app', function() {
	var bro = browserify(config.source.js)
		.transform(babelify, {presets: ['es2015', 'react']});

	externalLibs.forEach(function(lib) {
		bro.external(lib);
	});

	return bro.bundle()
		.pipe(source('app.js'))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(gulp.dest(config.dest.folder));
});

//Javascript vendor (browserify dependencies)
gulp.task('js-vendor', function () {
	var bro = browserify();
	externalLibs.forEach(function(lib) {
		bro.require(lib);
	});

	return bro.bundle()
		.pipe(source('vendor.js'))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(gulp.dest(config.dest.folder));
});

//App sass styling
gulp.task('sass', function() {
	return gulp.src(config.source.sass)
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(rename('style.css'))
		.pipe(gulp.dest(config.dest.folder));
});

gulp.task('watch', ['default'], function() {
	gulp.watch('js/**/*', ['js-app']);
	gulp.watch('sass/**/*', ['sass']);
});

//Default task with the right task execution order
gulp.task('default', ['js-vendor', 'js-app', 'sass']);