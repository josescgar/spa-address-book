SPA Address Book
================
Simple SPA address book built with **ReactJS** following the **Flux** architecture.

## Some of the tools used
- ReactJS
- HTML5 Web Storage API
- HTML5 Form validation
- Gulp
- Browserify
- Babelify
- Uglify
- SASS
- Twitter Bootstrap 3

## Usage
Just open the file `public/index.html` with the desired browser.

If you want to make changes to the sources, you will have to recompile the application. First install all the dependencies.
```
~/spa-address-book$: npm install
```
After the dependencies are installed, run the gulp task to compile the sources
```
~/spa-address-book$: gulp
```
